/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path='../external/node/node.d.ts' />
///<reference path='../external/socket.io/socket.io.d.ts' />

import Http = require('http');
import SocketIO = require('socket.io');

/** Una pequeña clase para encapsular los servicios
 *  básicos de un servidor de chat.
 */
class ChatServer
{
    private m_users: number;        //!< Cantidad de usuarios conectados
    private m_io: SocketIO.Server;  //!< Servidor de Socket.IO

    /** Crea una nueva instancia del servidor de chat.
     *
     *  @param[in] server
     *      El servidor donde se ejecutará.
     */
    public constructor(server: Http.Server)
    {
        this.m_users = 0;
        this.m_io = SocketIO(server);
    }

    /** Función que se ejecuta cuando se conecta un nuevo usuario.
     *
     *  @param[in] socket
     *      La información del nuevo usuario conectado.
     */
    private onUserConnected(socket: SocketIO.Socket): void
    {
        this.m_users += 1;

        if (this.m_users > 0)
        {
            this.m_io.emit('user connected', this.m_users);
        }

        // requistro los eventos que se emiten desde
        // el cliente y deben ser tratados, y distribuidos,
        // a los demás
        socket.on('disconnect', () => this.onUserDisconnected());
        socket.on('chat message', (msg) => this.onUserMessage(msg));
    }

    /** Función que se ejecuta cuando un usuario se desconecta.
     */
    private onUserDisconnected(): void
    {
        this.m_users -= 1;

        // informo a los demás usuarios conectados
        // (en caso de existir) que algún usuario
        // se desconectó para que actualize el
        // contador de usuarios en línea
        this.m_io.emit('user disconnected', this.m_users);
    }

    /** Procesa el mensaje enviado por un usuario
     *  y lo distribuye a los demás usuarios conectados.
     *
     *  @param[in] msg
     *      El mensaje que se debe procesar
     */
    private onUserMessage(msg: string)
    {
        // redistribuyo el mensaje a todos
        // los clientes conectados
        this.m_io.emit('chat message', msg);
    }

    public start(): void
    {
        // configuro las llamadas a socket
        this.m_io.sockets.on('connection', (socket) => this.onUserConnected(socket));
    }

}; // class ChatServer

export = ChatServer;
