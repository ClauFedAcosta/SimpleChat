/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path='../external/node/node.d.ts' />
///<reference path='../external/express/express.d.ts' />

import Http = require('http');
import Express = require('express');
import ChatServer = require('./ChatServer');

/** Esta clase provee una implementación sencilla
 *  y útil para desplegar un servidor sobre NodeJS.
 */
class Server
{
    private m_app: Express.Express;     //!< Aplicación principal
    private m_server: Http.Server;      //!< Instancia del servidor HTTP
    private m_port: number;             //!< Puerto donde se servirá
    private m_ip: string;               //!< IP donde se servirá
    private m_chat_server: ChatServer;  //!< El servidor de chat

    /** Crea una nueva instancia del servidor.
     *
     *  @param[in] port
     *      Puerto donde se escuchará el servidor
     *  @param[in] ip
     *      IP sobre la cual arrancará el servidor
     */
    public constructor(port: number, ip: string)
    {
        // instancio e inicializo los recursos
        // necesarios por el servidor
        this.m_app = Express();
        this.m_server = Http.createServer(this.m_app);
        this.m_port = port;
        this.m_ip = ip;
        this.m_chat_server = new ChatServer(this.m_server);
    }

    /** Muestra información sobre el servidor
     */
    private showInfo(): void
    {
        // muestro información sobre el servidor
        console.log("Servidor SimpleChat iniciado");
        console.log("Sirviendo en " + this.m_ip + ": " + this.m_port);
        console.log();
    }

    /** Inicia el servidor
     */
    public run(): void
    {
        // configuro la carpeta de recursos estáticos
        this.m_app.use(Express.static(__dirname + "/client"));

        // inicializo el servidor
        this.m_server.listen(this.m_port, this.m_ip, () => this.showInfo());

        // inicializo el servidor de chat
        this.m_chat_server.start();
    }

    /** Punto de entrada principal
     */
    public static main(): void
    {
        var server: Server;

        server = new Server(8080, '0.0.0.0');
        server.run();
    }

}; // class Server

// Función principal para iniciar
// el servidor.
Server.main();
