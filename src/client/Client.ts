/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path="../external/angularjs/angular.d.ts" />

import AngularJS = require('angular');
import ChatClient = require('./ChatClient');
import IClient = require('./IClient');

/** Clase base para la representación y manipulación
 *  de la interfaz gráfica de usuario.
 */
class Client implements IClient
{
    private m_scope: AngularJS.IScope;  //!< El ámbito del entorno AngularJS
    private m_chat: ChatClient;         //!< El cliente de chat
    public message: string;             //!< El mensaje de la interfaz de usuario

    /** Crea una nueva instancia de un cliente.
     *
     *  @param[in] scope
     *      El ámbito AngularJS de la aplicación.
     */
    public constructor(scope)
    {
        this.m_scope = scope;
        this.m_chat = new ChatClient(this);
        this.message = "";

        // inicializo el cliente de chat
        this.m_chat.start();

        // agrego la variable al ámbito AngularJS para
        // poder realizar una comunicación directa con
        // la interfaz gráfica de usuario
        this.m_scope.client = this;
    }

    /** Obtiene una cadena con la cantidad de usuarios
     *  en linea.
     *
     *  @return
     *      Un mensaje sencillo indicando la cantidad
     *      de usuarios conectados.
     */
    public getUsersConnected(): string
    {
        var count: number;

        count = this.m_chat.getUsersConnected();

        if (count > 1)
        {
            return "Hay " + count + " usuarios en línea";
        }
        else
        {
            return "Sos el único usuario en línea";
        }
    }

    /** Obtiene la lista de mensajes (historial) del chat.
     *
     *  @return
     *      Un array conteniendo las lineas del historial
     *      de chat.
     */
    public getMessagesList(): string[]
    {
        return this.m_chat.getHistory();
    }

    /** Método de llamada para enviar un mensaje al
     *  servidor.
     *
     *  Este método realiza el envío del mensaje indicado
     *  por el usuario hacia el servidor.
     */
    protected sendMessage(): void
    {
        if (this.message != "")
        {
            this.m_chat.sendMessage(this.message);
            this.message = "";
        }
    }

    /** Realiza la actualización de la interfaz gráfica de
     *  usuario.
     */
    public refresh(): void
    {
        this.m_scope.$apply();
    }

    /** Punto de entrada principal de la aplicación.
     */
    public static main(): void
    {
        // registro los requisitos para AngularJS y declaro
        // el controlador de la aplicación
        var app = AngularJS.module('SimpleChat', []);
        app.controller('Client', ['$scope', Client]);
    }

}; // class Client

Client.main();
