/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path="../external/socket.io/socket.io.d.ts" />

import SocketIO = require('socket.io');
import IClient = require('./IClient');

/** Pequeña clase que encapsula los servicios básicos
 *  necesarios para el despliegue de un cliente de chat.
 */
class ChatClient
{
    private m_users: number;            //!< Cantidad de usuarios conectados
    private m_history: string[];        //!< Historial del chat
    private m_socket: SocketIO.Server;  //!< Instancia del cliente Socket.IO
    private m_client: IClient;          //!< Instancia del cliente de chat

    /** Crea una nueva instancia
     *
     *  @param[in] client
     *      El cliente de usuario donde se ejecutará.
     */
    public constructor(client: IClient)
    {
        this.m_users = 0;
        this.m_history = [];
        this.m_socket = SocketIO();
        this.m_client = client;
    }

    /** Obtiene la cantidad de usuarios conectados.
     *
     *  @return
     *      La cantidad de usuarios conectados en el
     *      momento de la consulta.
     */
    public getUsersConnected(): number
    {
        return this.m_users;
    }

    /** Obtiene el historial de chat.
     *
     *  @return
     *      Un array con el historial de chat.
     */
    public getHistory(): string[]
    {
        return this.m_history;
    }

    /** Envía un mensaje al servidor del chat.
     *
     *  Esta método provee el mecanismo básico de comunicación
     *  con el servidor chat. Le informa de un nuevo mensaje y
     *  dependerá del servidor la acción a tomar.
     *
     *  @param[in] message
     *      La cadena de texto que se enviará al servidor.
     */
    public sendMessage(message: string): void
    {
        if (message != "")
        {
            this.m_socket.emit('chat message', message);
        }
    }

    /** Función de llamada para la actualización de
     *  la cantidad de usuarios.
     *
     *  Cuando un usuario se conecta o se desconecta, el servidor
     *  registra ese evento e informa a los demás clientes
     *  conectados para mantenerlos sincronizados. Éste es el
     *  método encargado de captar ese mensaje.
     *
     *  @param[in] number
     *      El número de usuarios actuales.
     */
    private onUpdateUsers(counter: number)
    {
        this.m_users = counter;
        this.m_client.refresh();
    }

    /** Procesa un evento de nuevo mensaje.
     *
     *  Este método es el encargado de recibir los mensajes
     *  desde el servidor y agregarlos al historial del cliente.
     *
     *  @param[in] message
     *      El mensaje que debe ser procesado.
     */
    private onMessage(message: string)
    {
        this.m_history.push(message);
        this.m_client.refresh();
    }

    /** Configura las llamadas de procesamiento e inicia
     *  el cliente de chat.
     */
    public start(): void
    {
        // registro de las funciones de eventos
        this.m_socket.on('user connected', (counter) => this.onUpdateUsers(counter));
        this.m_socket.on('user disconnected', (counter) => this.onUpdateUsers(counter));
        this.m_socket.on('chat message', (message) => this.onMessage(message));
    }

}; // class ChatClient

export = ChatClient;
