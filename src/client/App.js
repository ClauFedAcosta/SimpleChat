/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

// Este archivo es necesario para comunicarle a RequireJS
// las dependencias a usar y el "punto de entrada" de la
// aplicación.

// Lista de dependencias
require.config({
    paths: {
        // El servidor Socket.IO expone de manera automática
        // las dependencias necesarias por los clientes,
        // por lo cual no es necesario duplicar la biblioteca
        // en el lado del cliente.
        'socket.io': '/socket.io/socket.io',
        'jquery': 'lib/jquery',
        'bootstrap': 'lib/bootstrap',
        'angular': 'lib/angular'
    },
    shim: {
        // AngularJS no provee un mecanismo de compatibilidad
        // con el módulo AMD, por lo cual debo exportarlo
        // explícitamente
        'angular': {
            exports: 'angular'
        }
    }
});

// Punto de entrada de la aplicación
require(['Client']);
