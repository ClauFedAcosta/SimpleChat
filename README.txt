SimpleChat
==========

SimpleChat es un cliente y servidor de chat construído sobre NodeJS para el
back-end; usando además otras como AngularJS y Bootstrap para el diseño
front-end.

Compilación y prueba
====================

Para poder compilar el proyecto, son necesarias las siguientes dependencias:
    * NodeJS
    * NPM (incluído de serie con NodeJS)

Una vez adquirida la copia del código fuente, es necesario escribir en
consola los siguientes comandos (dentro de la carpeta del proyecto):

npm install
tsd reinstall
gulp
npm start

* "npm install" sirve para instalar las dependencias del proyecto.
* "tsd reinstall" se utiliza para descargar las definiciones de interfaces
    necesarios por el compilador TypeScript para poder utilizar bibliotecas
    escritas directamente en JavaScript de manera transparente.
* "gulp" se require para construir el proyecto completo.
* "npm start" inicia el servidor .

Bibliotecas y programas externos
================================

Además de NodeJS y NPM, este proyecto utiliza productos de terceros. La
lista completa de dependencias puede verse en el archivo "package.json" en
la carpeta raíz del proyecto.

